/**
 * Detail view of a Hexoskin Smart Device
 */
#import <UIKit/UIKit.h>
@import CoreBluetooth;
@import QuartzCore;

@interface DetailViewController : UITableViewController<CBPeripheralDelegate>

@property (strong, nonatomic) CBPeripheral *peripheral;

@end

