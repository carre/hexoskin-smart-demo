#import "DetailViewController.h"
#import "Constants.h"

@interface DetailViewController ()

@property CBUUID *hrSvcUUID;
@property CBUUID *respSvcUUID;
@property CBUUID *accSvcUUID;
@property CBUUID *hrCharUUID;
@property CBUUID *respCharUUID;
@property CBUUID *accCharUUID;

@property NSMutableArray *data;

- (NSString *)hexString:(NSData*)data;

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)viewDidLoad {
    [super viewDidLoad];
  
    // Peripheral name
    self.title = [self.peripheral name];
  
    // Set Controller as delegate of peripheral
    [self.peripheral setDelegate:self];
  
    // Heart Rate Service UUID
    self.hrSvcUUID = [CBUUID UUIDWithString:HEART_RATE_SERVICE_UUID];
  
    // Heart Rate Measurement UUID
    self.hrCharUUID = [CBUUID UUIDWithString:HEART_RATE_MEASUREMENT_UUID];
  
    // Respiration Service UUID
    self.respSvcUUID = [CBUUID UUIDWithString:RESPIRATION_RATE_SERVICE_UUID];
  
    // Respiration Measurement UUID
    self.respCharUUID = [CBUUID UUIDWithString:RESPIRATION_RATE_MEASUREMENT_UUID];
    
    // Accelerometer Service UUID
    self.accSvcUUID = [CBUUID UUIDWithString:ACCELEROMETER_SERVICE_UUID];
    
    // Accelerometer Measurement UUID
    self.accCharUUID = [CBUUID UUIDWithString:ACCELEROMETER_MEASUREMENT_UUID];
  
    // Data to display on table view
    self.data = [[NSMutableArray alloc] init];
  
    // Heart Rate Data
    [self.data addObject:@"HEART RATE --"];
  
    // Respiration Data
    [self.data addObject:@"RESP. RATE --"];
  
    // Inspiration and Expiration Data
    [self.data addObject:@"INSP/EXP "];
    
    // Total steps
    [self.data addObject:@"STEP COUNT --"];
    
    // Activity
    [self.data addObject:@"ACTIVITY --"];
    
    // Number of steps per minute
    [self.data addObject:@"CADENCE --"];
  
    // Discover Heart Rate and Respiration Services
    [self.peripheral discoverServices:@[self.hrSvcUUID, self.respSvcUUID, self.accSvcUUID]];
}

#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
  
    // Data string
    cell.textLabel.text = self.data[indexPath.row];
    return cell;
}

#pragma mark - CBPeripheralDelegate

// CBPeripheralDelegate - Invoked when you discover the peripheral's available services.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    for(CBService *svc in self.peripheral.services) {
        if([svc.UUID isEqual:self.hrSvcUUID]) {
            // Discover Heart Rate Measurement Characteristic
            [self.peripheral discoverCharacteristics:@[self.hrCharUUID] forService:svc];
        }
        else if([svc.UUID isEqual:self.respSvcUUID]) {
            // Discover Respiration Rate Measurement Characteristic
            [self.peripheral discoverCharacteristics:@[self.respCharUUID] forService:svc];
        }
        else if([svc.UUID isEqual:self.accSvcUUID]) {
            // Discover Accelerometer Measurement Characteristic
            [self.peripheral discoverCharacteristics:@[self.accCharUUID] forService:svc];
        }
    }
}

// Invoked when you discover the characteristics of a specified service.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if ([service.UUID isEqual:self.hrSvcUUID])  {
        for (CBCharacteristic *aChar in service.characteristics)
        {
            // Heart Rate
            // Subscribe to heart rate notification
            if ([aChar.UUID isEqual:self.hrCharUUID]) {
                [self.peripheral setNotifyValue:YES forCharacteristic:aChar];
            }
        }
    }
    else if ([service.UUID isEqual:self.respSvcUUID])  {
        for (CBCharacteristic *aChar in service.characteristics)
        {
            // Resp Rate
            // Subscribe to respiration notification
            if ([aChar.UUID isEqual:self.respCharUUID]) {
                [self.peripheral setNotifyValue:YES forCharacteristic:aChar];
            }
        }
    }
    else if ([service.UUID isEqual:self.accSvcUUID]) {
        for (CBCharacteristic *aChar in service.characteristics)
        {
            // Acc
            // Subscribe to accelerometer notification
            if ([aChar.UUID isEqual:self.accCharUUID]) {
                [self.peripheral setNotifyValue:YES forCharacteristic:aChar];
            }
        }
    }
}

// Invoked when you retrieve a specified characteristic's value, or when the peripheral device notifies your app that the characteristic's value has changed.
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    NSData *data = [characteristic value];
    NSString *hexString = [self hexString:data];
    const uint8_t *reportData = [data bytes];
    
    // heart rate value
    if ([characteristic.UUID isEqual:self.hrCharUUID]) {
        uint16_t bpm = 0;
        
        if ((reportData[0] & 0x01) == 0) {
            bpm = reportData[1];
        }
        else {
            bpm = CFSwapInt16LittleToHost(*(uint16_t *)(&reportData[1]));
        }
      
        [self.data setObject:[NSString stringWithFormat:@"HEART RATE %i, (%@)", bpm, hexString] atIndexedSubscript:0];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }
  
    // respiration value
    else if([characteristic.UUID isEqual:self.respCharUUID]) {
        uint16_t rpm = 0;
        
        if ((reportData[0] & 0x01) == 0) {
            rpm = reportData[1];
        }
        else {
            rpm = CFSwapInt16LittleToHost(*(uint16_t *)(&reportData[1]));
        }
        
        [self.data setObject:[NSString stringWithFormat:@"RESP RATE %i, (%@)", rpm, hexString] atIndexedSubscript:1];
        [self.data setObject:@"INSP/EXP" atIndexedSubscript:2];
        
        bool isInspExpPresent = (reportData[0] & 0x02) != 0;
        if(isInspExpPresent) {
            int startOffset = 1 + (reportData[0] & 0x01) == 0? 1:2;
            bool inspFirst = (reportData[0] & 0x04) == 0;
            NSMutableString *sb = [NSMutableString stringWithString:@"INSP/EXP "];
            for(int i=startOffset;i<data.length;i+=2) {
                uint16_t inspExp = CFSwapInt16LittleToHost(*(uint16_t *)(&reportData[i]));
                float value = inspExp / 32.0f;
                
                if(inspFirst) {
                    [sb appendFormat:@"%f (I), ", value];
                    inspFirst = NO;
                }
                else {
                    [sb appendFormat:@"%f (E), ", value];
                    inspFirst = YES;
                }
            }
            
            [self.data setObject:sb atIndexedSubscript:2];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }
    
    // accelerometer values
    else if ([characteristic.UUID isEqual:self.accCharUUID]) {
        uint16_t step = 0;
        float activity = 0;
        uint16_t cadence = 0;
        int dataIndex = 1;

        bool isStepCountPresent = (reportData[0] & 0x01) != 0;
        bool isActivityPresent = (reportData[0] & 0x02) != 0;
        bool isCadencePresent = (reportData[0] & 0x04) != 0;
        
        if (isStepCountPresent) {
            step = CFSwapInt16LittleToHost(*(uint16_t *)(&reportData[dataIndex]));
            [self.data setObject:[NSString stringWithFormat:@"STEP COUNT %i, (%@)", step, hexString] atIndexedSubscript:3];
            dataIndex = dataIndex + 2;
        }
        if (isActivityPresent) {
            activity = (CFSwapInt16LittleToHost(*(uint16_t *)(&reportData[dataIndex])))/256.0f;
            [self.data setObject:[NSString stringWithFormat:@"ACTIVITY %f, (%@)", activity, hexString] atIndexedSubscript:4];
            dataIndex = dataIndex + 2;
        }
        if (isCadencePresent) {
            cadence = CFSwapInt16LittleToHost(*(uint16_t *)(&reportData[dataIndex]));
            [self.data setObject:[NSString stringWithFormat:@"CADENCE %i, (%@)", cadence, hexString] atIndexedSubscript:5];
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }
}

- (NSString *)hexString:(NSData*)data
{
    NSUInteger bytesCount = data.length;
    if (bytesCount) {
        const char *hexChars = "0123456789ABCDEF";
        const unsigned char *dataBuffer = data.bytes;
        char *chars = malloc(sizeof(char) * (bytesCount * 2 + 1));
        char *s = chars;
        for (unsigned i = 0; i < bytesCount; ++i) {
            *s++ = hexChars[((*dataBuffer & 0xF0) >> 4)];
            *s++ = hexChars[(*dataBuffer & 0x0F)];
            dataBuffer++;
        }
        *s = '\0';
        NSString *hexString = [NSString stringWithUTF8String:chars];
        free(chars);
        return hexString;
    }
    return @"";
}

@end
