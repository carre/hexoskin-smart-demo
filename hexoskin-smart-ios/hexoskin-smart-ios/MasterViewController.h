/**
 * View for scanning and displaying list of discovered Hexoskin
 * Smart Devices
 */
#import <UIKit/UIKit.h>
@import CoreBluetooth;
@import QuartzCore;

@interface MasterViewController : UITableViewController<CBCentralManagerDelegate>

- (IBAction)onScanClick:(id)sender;

@end

