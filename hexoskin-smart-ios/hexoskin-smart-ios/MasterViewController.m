#import "MasterViewController.h"
#import "DetailViewController.h"
#import "Constants.h"

@interface MasterViewController ()

@property NSMutableArray *objects;
@property CBPeripheral *peripheral;
@property CBCentralManager *centralManager;

@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    // Peripheral manager
    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
  
    // peripherals
    self.objects = [[NSMutableArray alloc] init];
    
    [self findAlreadyConnectedDevices];
}

- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  
  CBPeripheral *peripheral = self.peripheral;
  
  // Unsubscribe from peripheral notification
  if(peripheral) {
    for(CBService *svc in peripheral.services) {
      for(CBCharacteristic *aChar in svc.characteristics) {
        [peripheral setNotifyValue:NO forCharacteristic:aChar];
      }
    }
    
   [self.centralManager cancelPeripheralConnection:peripheral];
    self.peripheral = nil;
  }
}

// Invoke when click on scan
- (IBAction)onScanClick:(id)sender {
    CBUUID *hrSvcUUID = [CBUUID UUIDWithString:HEART_RATE_SERVICE_UUID];
    CBUUID *respSvcUUID = [CBUUID UUIDWithString:RESPIRATION_RATE_SERVICE_UUID];
    CBUUID *accSvcUUID = [CBUUID UUIDWithString:ACCELEROMETER_SERVICE_UUID];
  
    // scan for peripheral with Heart Rate and Respiration Services
    [self.centralManager scanForPeripheralsWithServices:@[hrSvcUUID, respSvcUUID, accSvcUUID] options:NULL];
}


#pragma mark - Segues

// Invoke when tap on table view cell
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
  
    // Get Selected Peripheral
    self.peripheral = (CBPeripheral*)self.objects[indexPath.row];
  
    // Connect to peripheral
    [self.centralManager connectPeripheral:self.peripheral options:nil];
    
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        [self.centralManager stopScan];
    
        // Set peripheral on detail controller
        DetailViewController *controller = (DetailViewController *)[segue destinationViewController];
        controller.peripheral = self.peripheral;
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}


#pragma mark - CBCentralManagerDelegate

// method called whenever you have successfully connected to the BLE peripheral
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    if(self.peripheral.state == CBPeripheralStateConnected) {
        // Show Detail controller
        [self performSegueWithIdentifier:@"showDetail" sender:nil];
    }
}

// CBCentralManagerDelegate - This is called with the CBPeripheral class as its main input parameter. This contains most of the information there is to know about a BLE peripheral.
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    [self addPeripheralToList:peripheral];
}

// method called whenever the device state changes.
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
  if([central state] == CBManagerStatePoweredOn) {

  }
}


#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    CBPeripheral *object = self.objects[indexPath.row];
    cell.textLabel.text = [object name];
    return cell;
}


#pragma mark - Peripheral management

- (void) findAlreadyConnectedDevices {
    NSArray *servicesUUIDS = [NSArray arrayWithObjects:[CBUUID UUIDWithString:HEART_RATE_SERVICE_UUID],[CBUUID UUIDWithString:RESPIRATION_RATE_SERVICE_UUID],[CBUUID UUIDWithString:RESPIRATION_RATE_SERVICE_UUID],nil];
    
    NSArray *foundObjects = [NSArray arrayWithArray:[self.centralManager retrieveConnectedPeripheralsWithServices:servicesUUIDS]];
    
    if ([foundObjects count] != 0) {
        for (CBPeripheral *object in foundObjects) {
            [self addPeripheralToList:object];
        }
    }
}

- (void) addPeripheralToList:(CBPeripheral*)peripheral {
    // Discovered peripheral, add it to the list
    if([self.objects containsObject:peripheral] == NO && [[peripheral name] hasPrefix:@"HX-"]) {
        [self.objects addObject:peripheral];
        [self.tableView reloadData];
    }
}

@end



