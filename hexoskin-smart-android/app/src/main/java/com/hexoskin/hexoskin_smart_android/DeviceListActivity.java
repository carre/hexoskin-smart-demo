package com.hexoskin.hexoskin_smart_android;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/**
 * Displays list of Hexoskin Devices
 */
public class DeviceListActivity extends AppCompatActivity {

    private static final int PERMISSION_ACCESS_FINE_LOCATION = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;
    private BluetoothAdapter _bluetoothAdapter;
    private Handler _uiHandler;
    private RecyclerView.Adapter _adapter;
    private MenuItem _menuItem;
    private ArrayList<BluetoothDevice> _devices = new ArrayList<>();
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     byte[] scanRecord) {
                    runOnUiThread(() -> {
                        if (device.getName() != null && device.getName().startsWith("HX")) {
                            boolean found = false;
                            for (BluetoothDevice d : _devices) {
                                if (d.getName().equals(device.getName())) {
                                    found = true;
                                    break;
                                }
                            }

                            if (!found) {
                                _devices.add(device);
                                _adapter.notifyDataSetChanged();
                            }
                        }
                    });
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        _bluetoothAdapter = ((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
        _uiHandler = new Handler();

        RecyclerView _recyclerView = findViewById(R.id.my_recycler_view);
        _recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager _layoutManager = new LinearLayoutManager(this);
        _recyclerView.setLayoutManager(_layoutManager);
        _adapter = new DeviceListAdapter();
        _recyclerView.setAdapter(_adapter);

        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ACCESS_FINE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[],
                                           int[] grantResults) {
        if (requestCode == PERMISSION_ACCESS_FINE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                _menuItem.setEnabled(true);
            } else {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Location not granted");
                builder.setMessage("Since location access has not been granted, this app will not be able to discover nearby bluetooth devices.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.show();

                _menuItem.setEnabled(false);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_device_list, menu);
        _menuItem = menu.findItem(R.id.scan);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.scan) {
            scanLeDevice();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Scan for BLE devices
    private void scanLeDevice() {
        // Clear devices
        _devices.clear();

        // reload table
        _adapter.notifyDataSetChanged();
        stopScan();

        // Stops scanning after a pre-defined scan period.
        _uiHandler.postDelayed(() -> stopScan(), SCAN_PERIOD);

        _menuItem.setEnabled(false);

        // scan
        _bluetoothAdapter.startLeScan(mLeScanCallback);
    }

    private void stopScan() {
        _menuItem.setEnabled(true);
        _bluetoothAdapter.stopLeScan(mLeScanCallback);
    }


    private class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.DeviceListItemViewHolder> {

        @Override
        public DeviceListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);
            return new DeviceListItemViewHolder(v);
        }

        @Override
        public void onBindViewHolder(DeviceListItemViewHolder holder, int position) {
            BluetoothDevice device = _devices.get(position);
            holder._textView.setText(device.getName());
            holder._device = device;
        }

        @Override
        public int getItemCount() {
            return _devices.size();
        }

        class DeviceListItemViewHolder extends RecyclerView.ViewHolder {
            TextView _textView;
            BluetoothDevice _device;

            DeviceListItemViewHolder(View v) {
                super(v);
                _textView = v.findViewById(R.id.row_item);
                v.findViewById(R.id.container).setOnClickListener(v1 -> {

                    stopScan();

                    // Show Device Detail
                    Intent intent = new Intent(getApplicationContext(), DeviceActivity.class);
                    intent.putExtra("Device", _device);
                    startActivity(intent);
                });
            }
        }
    }
}
